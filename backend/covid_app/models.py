from django.db import models


class Statistic(models.Model):
    date_value = models.DateField()
    country_code = models.CharField(max_length=3)
    confirmed = models.IntegerField()
    deaths = models.IntegerField()
    stringency_actual = models.FloatField(null=True)
    stringency = models.FloatField(null=True)

    def __str__(self):
        return self.id
