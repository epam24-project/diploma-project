from covid_app.models import Statistic
from django.http import JsonResponse
from datetime import date
import requests
from django.views.decorators.http import require_GET


@require_GET
def update_stats(request):
    url = 'https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/date-range/' + \
        date.min.replace(year=date.today().year).strftime(
            "%Y-%m-%d")+'/'+date.today().strftime("%Y-%m-%d")
    response = requests.get(url)
    data = response.json()
    stats = data['data']
    try:
        for i in list(stats.keys()):
            for j in list(stats[i].keys()):
                stat_data = Statistic(
                    date_value=stats[i][j]['date_value'],
                    country_code=stats[i][j]['country_code'],
                    confirmed=stats[i][j]['confirmed'],
                    deaths=stats[i][j]['deaths'],
                    stringency_actual=stats[i][j]['stringency_actual'],
                    stringency=stats[i][j]['stringency']
                )
                if (not Statistic.objects.filter(date_value=stats[i][j]['date_value'], country_code=stats[i][j]['country_code']).exists()) and (stats[i][j]['country_code'] in data['countries'][:10]):
                    stat_data.save()
    except Exception:
        return JsonResponse({'result': 'fail'})
    else:
        return JsonResponse({'result': 'ok'})


@require_GET
def get_stats(request):
    if 'date' in request.GET:
        date = request.GET['date']
        print(date)
    all_stats = list(Statistic.objects.values())
    return JsonResponse({'data': all_stats}, safe=False)
