from django.urls import path
from covid_app import views

urlpatterns = [
    path('update', views.update_stats, name="update_stats"),
    path('get', views.get_stats, name='get_stats')
]
