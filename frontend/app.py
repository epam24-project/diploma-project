from flask import Flask, render_template, redirect, url_for
import requests

app = Flask(__name__)


@app.route("/")
def index():
    response = requests.get('http://covid-app-backend:8000/get')
    return render_template('index.html', records=response.json()['data'])


@app.route("/update")
def fetch():
    requests.get('http://covid-app-backend:8000/update')
    return redirect(url_for('index'))


def create_app():
    return app
